﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RouteOptimize_WEB.Models;
using System.Configuration;
using FastMember;
using Newtonsoft.Json;
using System.IO;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;

namespace RouteOptimize_WEB.Controllers
{
    public class RouteController : Controller
    {
        private string TmsConnectionString = ConfigurationManager.ConnectionStrings["DISPOPRD"].ConnectionString;
        private static clsMySQLConnection mysqlCon = new clsMySQLConnection();

        //private int maxPerRoute = 59;
        private DataTable dtActualRoute = new DataTable();
        private DataTable dtActualRouteSingle = new DataTable();
        private DataTable dtOriginalOrder = new DataTable();
        private DataTable dtFirstMain = new DataTable();
        private DataTable dtFirstMainSingle = new DataTable();

        // GET: Route
        public ActionResult Index()
        {
            var data = mysqlCon.GetDataTable("select distinct orderdate from sampleorder group by orderdate order by orderdate desc", "sampleorder");
            return View(data.AsEnumerable().ToList());
        }

        public ActionResult List()
        {
            return View();
        }

        public ActionResult ReadExcel()
        {
            return View();
        }

        public ActionResult _RouteTable(int totalTruck = 43)
        {
            //DataTable dtActualRoute = new DataTable();
            //DataTable dtFirstMain = GetRouteData();

            //DataTable dtFirstMainSingle = new DataTable();
            //dtFirstMainSingle = dtFirstMain.Select("IsSingleRoute =1").CopyToDataTable();
            //dtFirstMain.AcceptChanges();

            ////Single route
            //var groupedData = (from b in dtFirstMainSingle.AsEnumerable()
            //                   group b by b.Field<string>("routecode_thunder") into g
            //                   //(x => new { Team = x.team, Status = x.status })
            //                   let list = g.ToList()
            //                   select new
            //                   {

            //                       IsRouteComplete = list.Count == maxPerRoute ? true : false,
            //                       MainRoute = g.Key,

            //                       GrandTotalStore = list.Count,
            //                       MainRouteStore = list.Count,
            //                       storeListlist = list.CopyToDataTable<DataRow>(),
            //                       LastHustle = g.Key,
            //                       SuggestRoute = string.Format("({0})", g.Key),
            //                       IsHeavyRoute = false,
            //                       IsSingleRoute = true,
            //                       RemainStore = string.Empty,
            //                       TotalQty = list.CopyToDataTable<DataRow>().Compute("SUM(QTY)", string.Empty)


            //                   }).OrderByDescending(t => t.GrandTotalStore)
            //                  .ToList();



            //using (var reader = ObjectReader.Create(groupedData))
            //{
            //    dtActualRoute.Load(reader);
            //    dtActualRoute.AcceptChanges();
            //}

            ////Multi route
            //var groupedData2 = (from b in dtFirstMain.AsEnumerable()
            //               group b by b.Field<string>("routecode_thunder") into g
            //               //(x => new { Team = x.team, Status = x.status })
            //               let list = g.ToList()
            //               select new
            //               {

            //                   /*0*/
            //                   IsRouteComplete = list.Count == maxPerRoute ? true : false,
            //                   /*1*/
            //                   MainRoute = g.Key,
            //                   /*2*/
            //                   GrandTotalStore = list.Count,
            //                   /*3*/
            //                   MainRouteStore = list.Count,
            //                   /*4*/
            //                   storeListlist = list.CopyToDataTable<DataRow>(),
            //                   /*5*/
            //                   LastHustle = g.Key,
            //                   /*6*/
            //                   SuggestRoute = string.Format("({0})", g.Key),
            //                   /*7*/
            //                   IsHeavyRoute = false,
            //                   /*8*/
            //                   IsSingleRoute = false,
            //                   /*9*/
            //                   RemainStore = string.Empty,
            //                   /*10*/
            //                   TotalQty = list.CopyToDataTable<DataRow>().Compute("SUM(QTY)", string.Empty)


            //               }).OrderByDescending(t => t.GrandTotalStore)
            //                  .Take(totalTruck - dtActualRoute.Rows.Count)
            //                  .ToList();


            //using (var reader = ObjectReader.Create(groupedData2))
            //{
            //    dtActualRoute.Load(reader);
            //    dtActualRoute.AcceptChanges();
            //}
            //int countRow = dtActualRoute.Rows.Count;

            //var data = dtActualRoute.AsEnumerable().OrderByDescending(o => o.ItemArray[2]).ToList();
            //return PartialView(data);
            return PartialView();
        }

        public ActionResult _UnplanRouteTable(int totalTruck = 43)
        {
         //   DataTable dtActualRoute = new DataTable();
         //   DataTable dtUnplanRoute = new DataTable();
         //   DataTable dtFirstMain = GetRouteData();

         //   DataTable dtFirstMainSingle = new DataTable();
         //   dtFirstMainSingle = dtFirstMain.Select("IsSingleRoute =1").CopyToDataTable();


         //   dtUnplanRoute = dtFirstMain.Select("routecode_thunder is null").CopyToDataTable();
         //   dtFirstMain = dtFirstMain.Select("IsSingleRoute <>1").CopyToDataTable();
         //   //filter out null prefer route
         //   /*foreach (DataRow row in drArrayFirst)
         //   {
         //       dtUnplanRoute.Rows.Add(row.ItemArray);
         //       row.Delete();
         //   }*/
         //   dtFirstMain.AcceptChanges();
         //   dtUnplanRoute.AcceptChanges();

         //   //Single route
         //   var groupedData = (from b in dtFirstMainSingle.AsEnumerable()
         //                      group b by b.Field<string>("routecode_thunder") into g
         //                      //(x => new { Team = x.team, Status = x.status })
         //                      let list = g.ToList()
         //                      select new
         //                      {

         //                          IsRouteComplete = list.Count == maxPerRoute ? true : false,
         //                          MainRoute = g.Key,

         //                          GrandTotalStore = list.Count,
         //                          MainRouteStore = list.Count,
         //                          storeListlist = list.CopyToDataTable<DataRow>(),
         //                          LastHustle = g.Key,
         //                          SuggestRoute = string.Format("({0})", g.Key),
         //                          IsHeavyRoute = false,
         //                          IsSingleRoute = true,
         //                          RemainStore = string.Empty,
         //                          TotalQty = list.CopyToDataTable<DataRow>().Compute("SUM(QTY)", string.Empty)


         //                      }).OrderByDescending(t => t.GrandTotalStore)
         //                     .ToList();



         //   using (var reader = ObjectReader.Create(groupedData))
         //   {
         //       dtActualRoute.Load(reader);
         //       dtActualRoute.AcceptChanges();
         //   }

         //   //Multi route
         //   var groupedData2 = (from b in dtFirstMain.AsEnumerable()
         //                       group b by b.Field<string>("routecode_thunder") into g
         //                       //(x => new { Team = x.team, Status = x.status })
         //                       let list = g.ToList()
         //                       select new
         //                       {

         //                           /*0*/
         //                           IsRouteComplete = list.Count == maxPerRoute ? true : false,
         //                           /*1*/
         //                           MainRoute = g.Key,
         //                           /*2*/
         //                           GrandTotalStore = list.Count,
         //                           /*3*/
         //                           MainRouteStore = list.Count,
         //                           /*4*/
         //                           storeListlist = list.CopyToDataTable<DataRow>(),
         //                           /*5*/
         //                           LastHustle = g.Key,
         //                           /*6*/
         //                           SuggestRoute = string.Format("({0})", g.Key),
         //                           /*7*/
         //                           IsHeavyRoute = false,
         //                           /*8*/
         //                           IsSingleRoute = false,
         //                           /*9*/
         //                           RemainStore = string.Empty,
         //                           /*10*/
         //                           TotalQty = list.CopyToDataTable<DataRow>().Compute("SUM(QTY)", string.Empty)


         //                       }).OrderByDescending(t => t.GrandTotalStore)
         //                     .Take(totalTruck - dtActualRoute.Rows.Count)
         //                     .ToList();


         //   using (var reader = ObjectReader.Create(groupedData2))
         //   {
         //       dtActualRoute.Load(reader);
         //       dtActualRoute.AcceptChanges();
         //   }

         //   #region 2


         //   //---------------------------------------------------------
         //   //2.Get second route
         //   //2.1 get the rest route 
         //   DataTable dtSeconGroup = new DataTable();
         //   // bool isRowClose = false;
         //   dtSeconGroup = dtFirstMain;
         //   foreach (var thunder in groupedData.Select(x => x.MainRoute).ToList())
         //   {
         //       DataRow[] drSec = dtSeconGroup.Select(string.Format("routecode_thunder='{0}'", thunder));
         //       foreach (var row in drSec) row.Delete();
         //       dtSeconGroup.AcceptChanges();
         //   }
         //   //filter out null prefer route
         //   DataRow[] drSecArray = dtSeconGroup.Select("prefer1='0' OR prefer1 is null");
         //   foreach (var row in drSecArray)
         //   {
         //       dtUnplanRoute.Rows.Add(row.ItemArray);
         //       row.Delete();
         //   }
         //   dtUnplanRoute.AcceptChanges();
         //   dtSeconGroup.AcceptChanges();

         //   if (dtSeconGroup.Rows.Count > 0)
         //   {
         //       var subTotal0 = dtSeconGroup.AsEnumerable()
         //           .GroupBy(g => g.Field<string>("routecode_thunder"))
         //   .Select(g => new { MainThunder = g.Key, SubTotal = g.Count() })
         //   .ToDictionary(t => t.MainThunder, t => t.SubTotal);

         //       dtSeconGroup.Columns.Add("SubTotal", typeof(decimal));
         //       dtUnplanRoute.Columns.Add("SubTotal", typeof(decimal));
         //       foreach (var row in dtSeconGroup.AsEnumerable())
         //       {
         //           row.SetField(columnName: "SubTotal", value: subTotal0[row.Field<string>("routecode_thunder")]);
         //       }
         //       var subTotal1 = dtSeconGroup.AsEnumerable()
         //          .GroupBy(g => g.Field<string>("prefer1"))
         //  .Select(g => new { MainThunder = g.Key, SubTotal = g.Count() })
         //  .ToDictionary(t => t.MainThunder, t => t.SubTotal);

         //       dtSeconGroup.Columns.Add("SubTotal1", typeof(decimal));
         //       dtUnplanRoute.Columns.Add("SubTotal1", typeof(decimal));
         //       foreach (var row in dtSeconGroup.AsEnumerable())
         //       {
         //           if (!string.IsNullOrEmpty(row[1].ToString()))
         //           {
         //               row.SetField(columnName: "SubTotal1", value: subTotal1[row.Field<string>("prefer1")]);
         //           }
         //       }



         //       //  dtSeconGroup.DefaultView.Sort = "SubTotal DESC";
         //       DataView dv = dtSeconGroup.DefaultView;

         //       //if (r2.Checked)
         //       //{
         //       //    dv.Sort = "SubTotal DESC";
         //       //    //dtSeconGroup = dv.ToTable();
         //       //    //dtSeconGroup = RoutingDecision(dtSeconGroup);
         //       //}
         //       //if (r1.Checked)
         //       //{
         //       //    dv.Sort = "SubTotal1 DESC";
         //       //    // dtSeconGroup = dv.ToTable();
         //       //    //dtSeconGroup = RoutingDecisionDownThenAcross(dtSeconGroup);
         //       //}
         //       dtSeconGroup = dv.ToTable();
         //       dtSeconGroup = RoutingDecision(dtSeconGroup);

         //   }
         //   //-------------------------------------------------------------------------------------------
         //   #endregion
         //   #region 3
         //   //---------------------------------------------------------
         //   //3.Get third route
         //   //3.1 get the rest route 
         //   DataTable dtThirdGroup = new DataTable();
         //   dtThirdGroup = dtSeconGroup;
         //   DataRow[] drArray3 = dtThirdGroup.Select("routecode_thunder ='X'");
         //   foreach (var row in drArray3) row.Delete();
         //   dtThirdGroup.AcceptChanges();
         //   //filter out null prefer route
         //   DataRow[] drThirdArray = dtThirdGroup.Select("prefer2 ='0' OR prefer2 is null");
         //   foreach (var row in drThirdArray)
         //   {
         //       dtUnplanRoute.Rows.Add(row.ItemArray);
         //       row.Delete();
         //   }
         //   dtUnplanRoute.AcceptChanges();
         //   dtThirdGroup.AcceptChanges();
         //   if (dtThirdGroup.Rows.Count > 0)
         //   {//3.2 map third route to secound route
         //       var subTotal2 = dtThirdGroup.AsEnumerable()
         //        .GroupBy(g => string.IsNullOrEmpty(g.Field<string>("prefer2")) ? "X" : g.Field<string>("prefer2"))
         //.Select(g => new { MainThunder = g.Key, SubTotal = g.Count() })
         //.ToDictionary(t => t.MainThunder, t => t.SubTotal);

         //       dtThirdGroup.Columns.Add("SubTotal2", typeof(decimal));
         //       dtUnplanRoute.Columns.Add("SubTotal2", typeof(decimal));
         //       foreach (var row in dtThirdGroup.AsEnumerable())
         //       {
         //           if (!string.IsNullOrEmpty(row[2].ToString()))
         //           {
         //               row.SetField(columnName: "SubTotal2", value: subTotal2[row.Field<string>("prefer2")]);
         //           }
         //       }
         //       DataView dv = dtThirdGroup.DefaultView;

         //       //if (r2.Checked)
         //       //{
         //       //    dv.Sort = "SubTotal DESC";
         //       //    //dtSeconGroup = dv.ToTable();
         //       //    //dtSeconGroup = RoutingDecision(dtSeconGroup);
         //       //}
         //       //if (r1.Checked)
         //       //{
         //       //    dv.Sort = "SubTotal2 DESC";
         //       //    // dtSeconGroup = dv.ToTable();
         //       //    //dtSeconGroup = RoutingDecisionDownThenAcross(dtSeconGroup);
         //       //}
         //       dtThirdGroup = dv.ToTable();
         //       dtThirdGroup = RoutingDecision(dtThirdGroup);

         //   }
         //   //-------------------------------------------------------------------------------------------
         //   #endregion
         //   #region 4
         //   //---------------------------------------------------------
         //   //4.Get fourth route
         //   //4.1 get the rest route 
         //   DataTable dtFourthGroup = new DataTable();
         //   dtFourthGroup = dtThirdGroup;
         //   DataRow[] drArray4 = dtFourthGroup.Select("routecode_thunder ='X'");
         //   foreach (var row in drArray4) row.Delete();
         //   dtFourthGroup.AcceptChanges();
         //   //filter out null prefer route
         //   DataRow[] drFourthArray = dtFourthGroup.Select("prefer3 ='0' OR prefer3 is null");
         //   foreach (var row in drFourthArray)
         //   {
         //       dtUnplanRoute.Rows.Add(row.ItemArray);
         //       row.Delete();
         //   }
         //   dtUnplanRoute.AcceptChanges();
         //   dtFourthGroup.AcceptChanges();
         //   if (dtFourthGroup.Rows.Count > 0)
         //   {//4.2 map third route to secound route

         //       var subTotal3 = dtFourthGroup.AsEnumerable()
         //        .GroupBy(g => string.IsNullOrEmpty(g.Field<string>("prefer3")) ? "X" : g.Field<string>("prefer3"))
         //.Select(g => new { MainThunder = g.Key, SubTotal = g.Count() })
         //.ToDictionary(t => t.MainThunder, t => t.SubTotal);

         //       dtFourthGroup.Columns.Add("SubTotal3", typeof(decimal));
         //       dtUnplanRoute.Columns.Add("SubTotal3", typeof(decimal));
         //       foreach (var row in dtFourthGroup.AsEnumerable())
         //       {
         //           if (!string.IsNullOrEmpty(row[3].ToString()))
         //           {
         //               row.SetField(columnName: "SubTotal3", value: subTotal3[row.Field<string>("prefer3")]);
         //           }
         //       }
         //       DataView dv = dtFourthGroup.DefaultView;

         //       //if (r2.Checked)
         //       //{
         //       //    dv.Sort = "SubTotal DESC";
         //       //    //dtSeconGroup = dv.ToTable();
         //       //    //dtSeconGroup = RoutingDecision(dtSeconGroup);
         //       //}
         //       //if (r1.Checked)
         //       //{
         //       //    dv.Sort = "SubTotal3 DESC";
         //       //    // dtSeconGroup = dv.ToTable();
         //       //    //dtSeconGroup = RoutingDecisionDownThenAcross(dtSeconGroup);
         //       //}
         //       dtFourthGroup = dv.ToTable();
         //       dtFourthGroup = RoutingDecision(dtFourthGroup);

         //   }
         //   //-------------------------------------------------------------------------------------------
         //   #endregion
         //   #region 5


         //   //---------------------------------------------------------
         //   //5.Get five route
         //   //5.1 get the rest route 
         //   DataTable dtFiveGroup = new DataTable();
         //   dtFiveGroup = dtFourthGroup;
         //   DataRow[] drArray5 = dtFiveGroup.Select("routecode_thunder ='X'");
         //   foreach (var row in drArray5) row.Delete();
         //   dtFiveGroup.AcceptChanges();
         //   //filter out null prefer route
         //   DataRow[] drFiveArray = dtFiveGroup.Select("prefer4 ='0' OR prefer4 is null");
         //   foreach (var row in drFiveArray)
         //   {
         //       dtUnplanRoute.Rows.Add(row.ItemArray);
         //       row.Delete();
         //   }
         //   dtUnplanRoute.AcceptChanges();
         //   dtFiveGroup.AcceptChanges();
         //   if (dtFiveGroup.Rows.Count > 0)
         //   {
         //       var subTotal4 = dtFiveGroup.AsEnumerable()
         //        .GroupBy(g => string.IsNullOrEmpty(g.Field<string>("prefer4")) ? "X" : g.Field<string>("prefer4"))
         //.Select(g => new { MainThunder = g.Key, SubTotal = g.Count() })
         //.ToDictionary(t => t.MainThunder, t => t.SubTotal);

         //       dtFiveGroup.Columns.Add("SubTotal4", typeof(decimal));
         //       dtUnplanRoute.Columns.Add("SubTotal4", typeof(decimal));
         //       foreach (var row in dtFiveGroup.AsEnumerable())
         //       {
         //           if (!string.IsNullOrEmpty(row[4].ToString()))
         //           {
         //               row.SetField(columnName: "SubTotal4", value: subTotal4[row.Field<string>("prefer4")]);
         //           }
         //       }
         //       DataView dv = dtFiveGroup.DefaultView;

         //       //if (r2.Checked)
         //       //{
         //       //    dv.Sort = "SubTotal DESC";
         //       //    //dtSeconGroup = dv.ToTable();
         //       //    //dtSeconGroup = RoutingDecision(dtSeconGroup);
         //       //}
         //       //if (r1.Checked)
         //       //{
         //       //    dv.Sort = "SubTotal4 DESC";
         //       //    // dtSeconGroup = dv.ToTable();
         //       //    //dtSeconGroup = RoutingDecisionDownThenAcross(dtSeconGroup);
         //       //}
         //       dtFiveGroup = dv.ToTable();
         //       dtFiveGroup = RoutingDecision(dtFiveGroup);

         //   }
         //   DataRow[] drArrayLast = dtFiveGroup.Select("routecode_thunder ='X'");
         //   foreach (var row in drArrayLast) row.Delete();
         //   dtFiveGroup.AcceptChanges();
         //   //filter out null prefer route
         //   foreach (DataRow row in dtFiveGroup.Rows)
         //   {
         //       dtUnplanRoute.Rows.Add(row.ItemArray);
         //       row.Delete();
         //   }
         //   dtFiveGroup.AcceptChanges();
         //   dtUnplanRoute.AcceptChanges();
         //   //-------------------------------------------------------------------------------------------
         //   #endregion

            return PartialView();
        }

        //[HttpPost]
        public ActionResult GetRoute(int totalTruck, int maxPerRoute, string orderdate, int rdGroup, int cbAllowMaxStore, int cbAllowRemainStore, int txtMaxStore, int? flag = 0)
        {
            dtActualRoute = new DataTable();
            dtActualRouteSingle = new DataTable();
            DataTable dtUnplanRoute = new DataTable();

            dtFirstMain = GetRouteData(orderdate);
            DataTable dtFirstMainSingle = new DataTable();
            dtFirstMainSingle = dtFirstMain.Select("IsSingleRoute =1").CopyToDataTable();


            dtUnplanRoute = dtFirstMain.Select("routecode_thunder is null").CopyToDataTable();
            dtFirstMain = dtFirstMain.Select("IsSingleRoute <>1").CopyToDataTable();

            dtOriginalOrder = dtFirstMain.Copy();
            //filter out null prefer route
            /*foreach (DataRow row in drArrayFirst)
            {
                dtUnplanRoute.Rows.Add(row.ItemArray);
                row.Delete();
            }*/
            dtFirstMain.AcceptChanges();
            dtUnplanRoute.AcceptChanges();

            //Single route
            var groupedData = (from b in dtFirstMainSingle.AsEnumerable()
                               group b by b.Field<string>("routecode_thunder") into g
                               //(x => new { Team = x.team, Status = x.status })
                               let list = g.ToList()
                               select new
                               {

                                   IsRouteComplete = list.Count == maxPerRoute ? true : false,
                                   MainRoute = g.Key,

                                   GrandTotalStore = list.Count,
                                   MainRouteStore = list.Count,
                                   storeListlist = list.CopyToDataTable<DataRow>(),
                                   LastHustle = g.Key,
                                   SuggestRoute = string.Format("({0})", g.Key),
                                   IsHeavyRoute = false,
                                   IsSingleRoute = true,
                                   RemainStore = string.Empty,
                                   TotalQty = list.CopyToDataTable<DataRow>().Compute("SUM(QTY)", string.Empty),
                                   
                                   StoreName = list.Select(s => s.ItemArray[8]).FirstOrDefault(),

                                   SubDistrict = list.Select(s => s.ItemArray[9]).FirstOrDefault(),

                                   District = list.Select(s => s.ItemArray[10]).FirstOrDefault(),

                                   StoreCode = list.Select(s => s.ItemArray[11]).FirstOrDefault(),

                                   bData = string.Empty


                               }).OrderByDescending(t => t.GrandTotalStore)
                              .ToList();



            using (var reader = ObjectReader.Create(groupedData))
            {
                dtActualRoute.Load(reader);
                dtActualRoute.AcceptChanges();
            }

            //Multi route
            var groupedData_multi = (from b in dtFirstMain.AsEnumerable()
                                group b by b.Field<string>("routecode_thunder") into g
                                //(x => new { Team = x.team, Status = x.status })
                                let list = g.ToList()
                                select new
                                {
                                    /*0*/
                                    IsRouteComplete = list.Count == maxPerRoute ? true : false,
                                    /*1*/
                                    MainRoute = g.Key,
                                    /*2*/
                                    GrandTotalStore = list.Count,
                                    /*3*/
                                    MainRouteStore = list.Count,
                                    /*4*/
                                    storeListlist = list.CopyToDataTable<DataRow>(),
                                    /*5*/
                                    LastHustle = g.Key,
                                    /*6*/
                                    SuggestRoute = string.Format("({0})", g.Key),
                                    /*7*/
                                    IsHeavyRoute = false,
                                    /*8*/
                                    IsSingleRoute = false,
                                    /*9*/
                                    RemainStore = string.Empty,
                                    /*10*/
                                    TotalQty = list.CopyToDataTable<DataRow>().Compute("SUM(QTY)", string.Empty),

                                    StoreName = list.Select(s => s.ItemArray[8]).FirstOrDefault(),

                                    SubDistrict = list.Select(s => s.ItemArray[9]).FirstOrDefault(),

                                    District = list.Select(s => s.ItemArray[10]).FirstOrDefault(),

                                    StoreCode = list.Select(s => s.ItemArray[11]).FirstOrDefault(),

                                    bData = string.Empty


                                }).OrderByDescending(t => t.GrandTotalStore)
                              .Take(totalTruck - dtActualRoute.Rows.Count)
                              .ToList();
            
            var data = groupedData.AsEnumerable().Select(s => new {
                /*0*/
                s.IsRouteComplete,
                /*1*/
                s.MainRoute,
                /*2*/
                s.GrandTotalStore,
                /*3*/
                s.MainRouteStore,
                /*4*/
                s.storeListlist,
                /*5*/
                s.LastHustle,
                /*6*/
                s.SuggestRoute,
                /*7*/
                s.IsHeavyRoute,
                /*8*/
                s.IsSingleRoute,
                /*9*/
                s.RemainStore,
                /*10*/
                s.TotalQty,

                s.StoreName,

                s.SubDistrict,

                s.District,

                s.StoreCode,

                s.bData
            }).Concat(groupedData_multi).ToList();

            using (var reader = ObjectReader.Create(groupedData_multi))
            {
                dtActualRoute.Load(reader);
                dtActualRoute.AcceptChanges();
            }

            #region 2


            //---------------------------------------------------------
            //2.Get second route
            //2.1 get the rest route 
            DataTable dtSeconGroup = new DataTable();
            // bool isRowClose = false;
            dtSeconGroup = dtFirstMain;
            foreach (var thunder in groupedData.Select(x => x.MainRoute).ToList())
            {
                DataRow[] drSec = dtSeconGroup.Select(string.Format("routecode_thunder='{0}'", thunder));
                foreach (var row in drSec) row.Delete();
                dtSeconGroup.AcceptChanges();
            }
            //filter out null prefer route
            DataRow[] drSecArray = dtSeconGroup.Select("prefer1='0' OR prefer1 is null");
            foreach (var row in drSecArray)
            {
                dtUnplanRoute.Rows.Add(row.ItemArray);
                row.Delete();
            }
            dtUnplanRoute.AcceptChanges();
            dtSeconGroup.AcceptChanges();

            if (dtSeconGroup.Rows.Count > 0)
            {
                var subTotal0 = dtSeconGroup.AsEnumerable()
                    .GroupBy(g => g.Field<string>("routecode_thunder"))
            .Select(g => new { MainThunder = g.Key, SubTotal = g.Count() })
            .ToDictionary(t => t.MainThunder, t => t.SubTotal);

                dtSeconGroup.Columns.Add("SubTotal", typeof(decimal));
                dtUnplanRoute.Columns.Add("SubTotal", typeof(decimal));
                foreach (var row in dtSeconGroup.AsEnumerable())
                {
                    row.SetField(columnName: "SubTotal", value: subTotal0[row.Field<string>("routecode_thunder")]);
                }
                var subTotal1 = dtSeconGroup.AsEnumerable()
                   .GroupBy(g => g.Field<string>("prefer1"))
           .Select(g => new { MainThunder = g.Key, SubTotal = g.Count() })
           .ToDictionary(t => t.MainThunder, t => t.SubTotal);

                dtSeconGroup.Columns.Add("SubTotal1", typeof(decimal));
                dtUnplanRoute.Columns.Add("SubTotal1", typeof(decimal));
                foreach (var row in dtSeconGroup.AsEnumerable())
                {
                    if (!string.IsNullOrEmpty(row[1].ToString()))
                    {
                        row.SetField(columnName: "SubTotal1", value: subTotal1[row.Field<string>("prefer1")]);
                    }
                }



                //  dtSeconGroup.DefaultView.Sort = "SubTotal DESC";
                DataView dv = dtSeconGroup.DefaultView;

                if (rdGroup == 2)
                {
                    dv.Sort = "SubTotal DESC";
                    //dtSeconGroup = dv.ToTable();
                    //dtSeconGroup = RoutingDecision(dtSeconGroup);
                }
                if (rdGroup == 1)
                {
                    dv.Sort = "SubTotal1 DESC";
                    // dtSeconGroup = dv.ToTable();
                    //dtSeconGroup = RoutingDecisionDownThenAcross(dtSeconGroup);
                }
                dtSeconGroup = dv.ToTable();
                dtSeconGroup = RoutingDecision(dtSeconGroup, maxPerRoute, cbAllowMaxStore);

            }
            //-------------------------------------------------------------------------------------------
            #endregion
            #region 3
            //---------------------------------------------------------
            //3.Get third route
            //3.1 get the rest route 
            DataTable dtThirdGroup = new DataTable();
            dtThirdGroup = dtSeconGroup;
            DataRow[] drArray3 = dtThirdGroup.Select("routecode_thunder ='X'");
            foreach (var row in drArray3) row.Delete();
            dtThirdGroup.AcceptChanges();
            //filter out null prefer route
            DataRow[] drThirdArray = dtThirdGroup.Select("prefer2 ='0' OR prefer2 is null");
            foreach (var row in drThirdArray)
            {
                dtUnplanRoute.Rows.Add(row.ItemArray);
                row.Delete();
            }
            dtUnplanRoute.AcceptChanges();
            dtThirdGroup.AcceptChanges();
            if (dtThirdGroup.Rows.Count > 0)
            {//3.2 map third route to secound route
                var subTotal2 = dtThirdGroup.AsEnumerable()
                 .GroupBy(g => string.IsNullOrEmpty(g.Field<string>("prefer2")) ? "X" : g.Field<string>("prefer2"))
         .Select(g => new { MainThunder = g.Key, SubTotal = g.Count() })
         .ToDictionary(t => t.MainThunder, t => t.SubTotal);

                dtThirdGroup.Columns.Add("SubTotal2", typeof(decimal));
                dtUnplanRoute.Columns.Add("SubTotal2", typeof(decimal));
                foreach (var row in dtThirdGroup.AsEnumerable())
                {
                    if (!string.IsNullOrEmpty(row[2].ToString()))
                    {
                        row.SetField(columnName: "SubTotal2", value: subTotal2[row.Field<string>("prefer2")]);
                    }
                }
                DataView dv = dtThirdGroup.DefaultView;

                if (rdGroup == 2)
                {
                    dv.Sort = "SubTotal DESC";
                    //dtSeconGroup = dv.ToTable();
                    //dtSeconGroup = RoutingDecision(dtSeconGroup);
                }
                if (rdGroup == 1)
                {
                    dv.Sort = "SubTotal2 DESC";
                    // dtSeconGroup = dv.ToTable();
                    //dtSeconGroup = RoutingDecisionDownThenAcross(dtSeconGroup);
                }
                dtThirdGroup = dv.ToTable();
                dtThirdGroup = RoutingDecision(dtThirdGroup, maxPerRoute, cbAllowMaxStore);

            }
            //-------------------------------------------------------------------------------------------
            #endregion
            #region 4
            //---------------------------------------------------------
            //4.Get fourth route
            //4.1 get the rest route 
            DataTable dtFourthGroup = new DataTable();
            dtFourthGroup = dtThirdGroup;
            DataRow[] drArray4 = dtFourthGroup.Select("routecode_thunder ='X'");
            foreach (var row in drArray4) row.Delete();
            dtFourthGroup.AcceptChanges();
            //filter out null prefer route
            DataRow[] drFourthArray = dtFourthGroup.Select("prefer3 ='0' OR prefer3 is null");
            foreach (var row in drFourthArray)
            {
                dtUnplanRoute.Rows.Add(row.ItemArray);
                row.Delete();
            }
            dtUnplanRoute.AcceptChanges();
            dtFourthGroup.AcceptChanges();
            if (dtFourthGroup.Rows.Count > 0)
            {//4.2 map third route to secound route

                var subTotal3 = dtFourthGroup.AsEnumerable()
                 .GroupBy(g => string.IsNullOrEmpty(g.Field<string>("prefer3")) ? "X" : g.Field<string>("prefer3"))
         .Select(g => new { MainThunder = g.Key, SubTotal = g.Count() })
         .ToDictionary(t => t.MainThunder, t => t.SubTotal);

                dtFourthGroup.Columns.Add("SubTotal3", typeof(decimal));
                dtUnplanRoute.Columns.Add("SubTotal3", typeof(decimal));
                foreach (var row in dtFourthGroup.AsEnumerable())
                {
                    if (!string.IsNullOrEmpty(row[3].ToString()))
                    {
                        row.SetField(columnName: "SubTotal3", value: subTotal3[row.Field<string>("prefer3")]);
                    }
                }
                DataView dv = dtFourthGroup.DefaultView;

                if (rdGroup == 2)
                {
                    dv.Sort = "SubTotal DESC";
                    //dtSeconGroup = dv.ToTable();
                    //dtSeconGroup = RoutingDecision(dtSeconGroup);
                }
                if (rdGroup == 1)
                {
                    dv.Sort = "SubTotal3 DESC";
                    // dtSeconGroup = dv.ToTable();
                    //dtSeconGroup = RoutingDecisionDownThenAcross(dtSeconGroup);
                }
                dtFourthGroup = dv.ToTable();
                dtFourthGroup = RoutingDecision(dtFourthGroup, maxPerRoute, cbAllowMaxStore);

            }
            //-------------------------------------------------------------------------------------------
            #endregion
            #region 5


            //---------------------------------------------------------
            //5.Get five route
            //5.1 get the rest route 
            DataTable dtFiveGroup = new DataTable();
            dtFiveGroup = dtFourthGroup;
            DataRow[] drArray5 = dtFiveGroup.Select("routecode_thunder ='X'");
            foreach (var row in drArray5) row.Delete();
            dtFiveGroup.AcceptChanges();
            //filter out null prefer route
            DataRow[] drFiveArray = dtFiveGroup.Select("prefer4 ='0' OR prefer4 is null");
            foreach (var row in drFiveArray)
            {
                dtUnplanRoute.Rows.Add(row.ItemArray);
                row.Delete();
            }
            dtUnplanRoute.AcceptChanges();
            dtFiveGroup.AcceptChanges();
            if (dtFiveGroup.Rows.Count > 0)
            {
                var subTotal4 = dtFiveGroup.AsEnumerable()
                 .GroupBy(g => string.IsNullOrEmpty(g.Field<string>("prefer4")) ? "X" : g.Field<string>("prefer4"))
         .Select(g => new { MainThunder = g.Key, SubTotal = g.Count() })
         .ToDictionary(t => t.MainThunder, t => t.SubTotal);

                dtFiveGroup.Columns.Add("SubTotal4", typeof(decimal));
                dtUnplanRoute.Columns.Add("SubTotal4", typeof(decimal));
                foreach (var row in dtFiveGroup.AsEnumerable())
                {
                    if (!string.IsNullOrEmpty(row[4].ToString()))
                    {
                        row.SetField(columnName: "SubTotal4", value: subTotal4[row.Field<string>("prefer4")]);
                    }
                }
                DataView dv = dtFiveGroup.DefaultView;

                if (rdGroup == 2)
                {
                    dv.Sort = "SubTotal DESC";
                    //dtSeconGroup = dv.ToTable();
                    //dtSeconGroup = RoutingDecision(dtSeconGroup);
                }
                if (rdGroup == 1)
                {
                    dv.Sort = "SubTotal4 DESC";
                    // dtSeconGroup = dv.ToTable();
                    //dtSeconGroup = RoutingDecisionDownThenAcross(dtSeconGroup);
                }
                dtFiveGroup = dv.ToTable();
                dtFiveGroup = RoutingDecision(dtFiveGroup, maxPerRoute, cbAllowMaxStore);

            }
            DataRow[] drArrayLast = dtFiveGroup.Select("routecode_thunder ='X'");
            foreach (var row in drArrayLast) row.Delete();
            dtFiveGroup.AcceptChanges();
            //filter out null prefer route
            foreach (DataRow row in dtFiveGroup.Rows)
            {
                dtUnplanRoute.Rows.Add(row.ItemArray);
                row.Delete();
            }
            dtFiveGroup.AcceptChanges();
            dtUnplanRoute.AcceptChanges();
            //-------------------------------------------------------------------------------------------
            #endregion

            if (flag == 1)
            {
                var query = data.AsEnumerable().Select(s => new
                {
                    PickSlipNo = "",
                    s.StoreCode,
                    s.StoreName,
                    s.SubDistrict,
                    s.District,
                    s.MainRoute,
                    s.TotalQty
                }).ToList();

                var query2 = data.AsEnumerable().Select((s, i) => new
                {
                    No = i + 1,
                    b1 = s.bData,
                    b2 = s.bData,
                    b3 = s.bData,
                    b4 = s.bData,
                    Loading = "Pallet Load",
                    b5 = s.bData,
                    MainRoute = s.MainRoute,
                    TotalQty = s.TotalQty,
                    TotalQty2 = s.TotalQty,
                    b6 = s.bData,
                    b7 = s.bData,
                    b8 = s.bData,
                    b9 = s.bData,
                    b10 = s.bData,
                    b11 = s.bData,
                    b12 = s.bData,
                    b13 = s.bData
                }).ToList();

                //ModifyExcel("~/Files", "Export_Template.xlsx", null, data_excel, data_excel_ws2, null, null, 1, 2);


                string fileName = "Export_Template.xlsx";
                string folder = "~/Files";
                int worksheetNumber = 2;
                int rowBeginHead = 1;
                List<string> header = null;

                using (ExcelPackage pkg = new ExcelPackage())
                {
                    var fullFilename = fileName.Contains(".xlsx") ? fileName : fileName + ".xlsx";
                    var oldFilePath = Server.MapPath((folder.Contains("~/") ? folder : "~/" + folder) + "/" + fullFilename);
                    using (FileStream stream = new FileStream(oldFilePath, FileMode.Open))
                    {
                        //ExcelWorksheet ws = null;
                        pkg.Load(stream);
                        for (int j = 1; j <= worksheetNumber; j++)
                        {
                            ExcelWorksheet ws = pkg.Workbook.Worksheets[j];

                            if (query != null && query.Count > 0)
                            {
                                //get our column headings
                                var colHeadCount = 0;
                                if (header != null && header.Count > 0)
                                {
                                    for (int i = 0; i < header.Count; i++)
                                    {
                                        ws.Cells[rowBeginHead, i + 1].Value = header[i];
                                        colHeadCount++;
                                    }

                                    //Format the header
                                    using (ExcelRange rng = ws.Cells[rowBeginHead, 1, rowBeginHead, colHeadCount])
                                    {
                                        rng.Style.Font.Bold = true;
                                        rng.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                        rng.Style.Border.Bottom.Style = rng.Style.Border.Left.Style = rng.Style.Border.Right.Style = rng.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                        rng.Style.Fill.PatternType = ExcelFillStyle.Solid;                      //Set Pattern for the background to Solid
                                        rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(79, 129, 189));  //Set color to dark blue
                                        rng.Style.Font.Color.SetColor(Color.White);
                                    }
                                }

                                //populate our Data from List
                                var rowBeginList = rowBeginHead + 1;
                                var range = j == 1 ? ws.Cells["A" + rowBeginList].LoadFromCollection(query) : ws.Cells["A" + rowBeginList].LoadFromCollection(query2);
                                range.Style.Border.Bottom.Style = range.Style.Border.Left.Style = range.Style.Border.Right.Style = range.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            }
                        }

                        //Write it back to the client
                        Random rand = new Random();
                        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        Response.AddHeader("content-disposition", "attachment;  filename=" + DateTime.Now.ToString("yyyyMMdd") + "_" + rand.Next(1, 999) + "_" + fullFilename);
                        Response.BinaryWrite(pkg.GetAsByteArray());
                        Response.End();

                    }
                }
            }

            JsonSerializerSettings jss = new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore };
            var result = JsonConvert.SerializeObject(data, Formatting.Indented, jss);
            var data_unplan = dtUnplanRoute.AsEnumerable().Select(s => new
            {
                col1 = s.ItemArray[0],
                col2 = s.ItemArray[1],
                col3 = s.ItemArray[2],
                col4 = s.ItemArray[3],
                col5 = s.ItemArray[4],
                col6 = s.ItemArray[5],
                col7 = s.ItemArray[6],
                col8 = s.ItemArray[7],
                col9 = s.ItemArray[8],
                col10 = s.ItemArray[9]
            }).ToList();
            var result_unplan = JsonConvert.SerializeObject(data_unplan, Formatting.Indented, jss);

            string textBox5 = dtActualRoute.Compute("Avg([GrandTotalStore])", "").ToString();
            string textBox7 = dtActualRoute.Compute("Max([GrandTotalStore])", "").ToString();
            string txtTotalZone = dtOriginalOrder.AsEnumerable()
                .Where(x => !string.IsNullOrEmpty(x.Field<string>("routecode_thunder")))
                .GroupBy(x => x.Field<string>("routecode_thunder")).Distinct().Count().ToString();
            string txtTotalStore = dtOriginalOrder.AsEnumerable()
                .Where(x => !string.IsNullOrEmpty(x.Field<string>("store_code")))
                .GroupBy(x => x.Field<string>("store_code")).Distinct().Count().ToString();
            string txtTotalQty = dtOriginalOrder.Compute("Sum([qty])", "").ToString();
            string txtPreferTruck = dtActualRoute.Compute("Count([SuggestRoute])", "").ToString();

            return Json(new
            {
                route = result,
                unplan_route = result_unplan,
                textBox5 = textBox5,
                textBox7 = textBox7,
                txtTotalZone = txtTotalZone,
                txtTotalStore = txtTotalStore,
                txtTotalQty = txtTotalQty,
                txtPreferTruck = txtPreferTruck
            });
        }

        public DataTable GetRouteData(string orderdate)
        {

            string strSql = string.Format(@"SELECT 
sm.routecode_thunder,
sm.prefer_1 AS 'prefer1',
sm.prefer_2 AS 'prefer2',
sm.prefer_3 AS 'prefer3',
sm.prefer_3 AS 'prefer4',
so.store_code,
SUM(qty) AS'qty',
sm.IsSingleRoute,
sm.storename,
sm.subdistrict,
sm.district,
sm.storecode

FROM sampleorder so
left JOIN day_plan  dp ON DAYOFWEEK(NOW()) =   dp.DayofWeekNumber
left JOIN store_master sm ON sm.storecode = so.store_code  
WHERE so.orderdate ='" + orderdate + @"'
-- AND sm.routecode_thunder IS NOT NULL
GROUP BY sm.routecode_thunder,so.store_code
");
            DataTable dt = null;
            using (MySqlCommand cmd = new MySqlCommand())
            {
                cmd.CommandText = strSql;
                cmd.CommandType = CommandType.Text;
                dt = new DataTable("route");
                DataColumn dc = new DataColumn("routecode_thunder", typeof(string));
                dt.Columns.Add(dc);
                dc = new DataColumn("store_code", typeof(string));
                dt.Columns.Add(dc);
                dc = new DataColumn("backuproute", typeof(string));
                dt.Columns.Add(dc);
                dc = new DataColumn("qty", typeof(int));
                dt.Columns.Add(dc);
                dt = (DataTable)this.TExcuteQuery(cmd, ReturnType.DataTable);
                cmd.Dispose();
            }

            return dt;
        }

        protected object TExcuteQuery(MySqlCommand cmd, ReturnType returnType)
        {

            using (MySqlConnection connection = new MySqlConnection(TmsConnectionString))
            {
                cmd.Connection = connection;
                cmd.CommandTimeout = 1000;
                Open(connection, this.TmsConnectionString);

                DataSet ds = new DataSet();
                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(ds);
                connection.Dispose();
                return ds.Tables[0];

            }

        }

        private void Open(MySqlConnection connection, string connectionString)
        {
            try
            {
                connection.Open();
            }
            finally
            {
                if (connection.State != ConnectionState.Open)
                {
                    //System.Threading.Thread.Sleep(10000);//1 minute
                    var connection1 = new MySqlConnection(connectionString);
                    connection1.Open();
                    connection.Close();
                }
            }
        }

        /// <summary>
        /// Execute sql
        /// </summary>
        /// <param name="cmd"></param>
        /// <returns></returns>
        protected int TExecuteScalar(MySqlCommand cmd)
        {
            var returnV = 0;
            using (MySqlConnection connection = new MySqlConnection(TmsConnectionString))
            {
                cmd.Connection = connection;
                Open(connection, this.TmsConnectionString);

                returnV = (Int32)cmd.ExecuteNonQuery();

                cmd.Dispose();
                connection.Dispose();
            }
            return (int)returnV;
        }
        private DataTable RoutingDecision(DataTable remainStore, int maxPerRoute, int chkAllowMaxStore)
        {
            bool isRowClose = false;

            //2.2 map second route to first route
            DataRowCollection drRemainStore = remainStore.Rows;
            for (int i = 0; i <= drRemainStore.Count - 1; i++)
            {
                isRowClose = false;
                for (int p = 0; p <= 3; p++)
                {
                    if (!isRowClose)
                    {
                        string criteria = string.Empty;


                        criteria =
                            chkAllowMaxStore > 0 ?
                        string.Format("SuggestRoute like '%{0}%' ", drRemainStore[i][string.Format("prefer{0}", p + 1)]) :
                        string.Format("SuggestRoute like '%{0}%' AND IsRouteComplete = false ", drRemainStore[i][string.Format("prefer{0}", p + 1)]);


                        DataRow[] drActualRoute = dtActualRoute.Select(criteria);

                        if (drActualRoute != null)
                        {
                            if (drActualRoute.Count() > 0 && drActualRoute.Count() == 1)
                            {

                                List<string> hustleList = new List<string>();
                                foreach (var rowActual in drActualRoute)
                                {
                                    rowActual.BeginEdit();
                                    if ((int)rowActual["GrandTotalStore"] < maxPerRoute || chkAllowMaxStore > 0 && !(bool)rowActual["IsSingleRoute"])
                                    {
                                        rowActual["LastHustle"] = drRemainStore[i]["routecode_thunder"];
                                        if (!rowActual["SuggestRoute"].ToString().Contains(drRemainStore[i]["routecode_thunder"].ToString()))
                                        {
                                            rowActual["SuggestRoute"] = string.Format("{0}+({1})", rowActual["SuggestRoute"], drRemainStore[i]["routecode_thunder"]);
                                        }

                                        //Add store to main route
                                        DataTable inner = (DataTable)drActualRoute[0]["storeListlist"];
                                        DataRow secondInner = inner.NewRow();
                                        secondInner["routecode_thunder"] = drRemainStore[i]["routecode_thunder"];
                                        secondInner["store_code"] = drRemainStore[i]["store_code"];
                                        secondInner["qty"] = drRemainStore[i]["qty"];
                                        inner.Rows.Add(secondInner);


                                        drRemainStore[i]["routecode_thunder"] = "X";
                                        rowActual["GrandTotalStore"] = (int)rowActual["GrandTotalStore"] + 1;
                                        rowActual["TotalQty"] = (decimal)rowActual["TotalQty"] + (decimal)drRemainStore[i]["qty"];
                                        if ((int)rowActual["GrandTotalStore"] == maxPerRoute)
                                        {
                                            rowActual["IsRouteComplete"] = (bool)true;
                                        }
                                        if ((int)rowActual["GrandTotalStore"] > maxPerRoute)
                                        {
                                            rowActual["IsHeavyRoute"] = (bool)true;
                                        }
                                        isRowClose = true;
                                    }
                                    rowActual.EndEdit();

                                }

                                dtActualRoute.AcceptChanges();
                            }
                        }

                    }
                }
            }
            return remainStore;
        }
        private DataTable RoutingDecisionDownThenAcross(DataTable remainStore, int maxPerRoute, int chkAllowMaxStore)
        {
            bool isRowClose = false;

            //2.2 map second route to first route
            DataRowCollection drRemainStore = remainStore.Rows; for (int p = 0; p <= 3; p++)
            {
                for (int i = 0; i <= drRemainStore.Count - 1; i++)
                {
                    isRowClose = false;
                    if (!isRowClose)
                    {
                        string criteria = string.Empty;


                        criteria =
                            chkAllowMaxStore > 0 ?
                        string.Format("SuggestRoute like '%{0}%' ", drRemainStore[i][string.Format("prefer{0}", p + 1)]) :
                        string.Format("SuggestRoute like '%{0}%' AND IsRouteComplete = false ", drRemainStore[i][string.Format("prefer{0}", p + 1)]);


                        DataRow[] drActualRoute = dtActualRoute.Select(criteria);

                        if (drActualRoute != null)
                        {
                            if (drActualRoute.Count() > 0 && drActualRoute.Count() == 1)
                            {

                                List<string> hustleList = new List<string>();
                                foreach (var rowActual in drActualRoute)
                                {
                                    rowActual.BeginEdit();
                                    if ((int)rowActual["GrandTotalStore"] < maxPerRoute || chkAllowMaxStore > 0 && !(bool)rowActual["IsSingleRoute"])
                                    {
                                        rowActual["LastHustle"] = drRemainStore[i]["routecode_thunder"];
                                        if (!rowActual["SuggestRoute"].ToString().Contains(drRemainStore[i]["routecode_thunder"].ToString()))
                                        {
                                            rowActual["SuggestRoute"] = string.Format("{0}+({1})", rowActual["SuggestRoute"], drRemainStore[i]["routecode_thunder"]);
                                        }

                                        //Add store to main route
                                        DataTable inner = (DataTable)drActualRoute[0]["storeListlist"];
                                        DataRow secondInner = inner.NewRow();
                                        secondInner["routecode_thunder"] = drRemainStore[i]["routecode_thunder"];
                                        secondInner["store_code"] = drRemainStore[i]["store_code"];
                                        secondInner["qty"] = drRemainStore[i]["qty"];
                                        inner.Rows.Add(secondInner);


                                        drRemainStore[i]["routecode_thunder"] = "X";
                                        rowActual["GrandTotalStore"] = (int)rowActual["GrandTotalStore"] + 1;
                                        rowActual["TotalQty"] = (decimal)rowActual["TotalQty"] + (decimal)drRemainStore[i]["qty"];
                                        if ((int)rowActual["GrandTotalStore"] == maxPerRoute)
                                        {
                                            rowActual["IsRouteComplete"] = (bool)true;
                                        }
                                        if ((int)rowActual["GrandTotalStore"] > maxPerRoute)
                                        {
                                            rowActual["IsHeavyRoute"] = (bool)true;
                                        }
                                        isRowClose = true;
                                    }
                                    rowActual.EndEdit();

                                }

                                dtActualRoute.AcceptChanges();
                            }
                        }

                    }
                }
            }
            return remainStore;
        }

        public ActionResult SaveImportRoute(HttpPostedFileBase fileRoute)
        {
            string ins_query = string.Empty;
            string file_path = Uploadfile2Temp(fileRoute);
            var readExcel = ReadDataFromExcel(file_path);

            if (CheckDuplicateOrderRoute(readExcel[0][0].ToString()) > 0)
                DeleteDuplicateOrderRoute(readExcel[0][0].ToString());

            foreach (var a in readExcel)
            {
                string orderdate_date = a[0].ToString().Substring(0, 4) + "-" + a[0].ToString().Substring(4, 2) + "-" + a[0].ToString().Substring(6, 2);
                ins_query += " INSERT INTO sampleorder (orderdate, orderdate_date, route_desc, po_no, store_code, matchcode, qty, tripcode)";
                ins_query += " VALUES";
                ins_query += " ('"+ a[0].ToString() + "',";
                ins_query += "'" + orderdate_date + "',";
                ins_query += "'" + a[1].ToString() + "',";
                ins_query += "'" + a[2].ToString() + "',";
                ins_query += "'" + a[3].ToString() + "',";
                ins_query += "'" + a[4].ToString() + "',";
                ins_query += "" + a[5].ToString() + ",";
                ins_query += "'" + a[6].ToString() + "'";
                ins_query += " );";
            }
            if (!string.IsNullOrEmpty(ins_query)) mysqlCon.Execute(ins_query);

            TempData["Success"] = "Import route optimize success !!";
            return RedirectToAction("Index");
        }

        public string Uploadfile2Temp(HttpPostedFileBase doc)
        {
            string subPath = "~/uploadfiles/" + DateTime.Now.Year + "/" + DateTime.Now.Month + "/";
            if (!Directory.Exists(Server.MapPath(subPath)))
            {
                Directory.CreateDirectory(Server.MapPath(subPath));
            }

            var dmy = DateTime.Now.Day.ToString("00") + "" + DateTime.Now.Month.ToString("00") + "" + DateTime.Now.Year.ToString().Substring(2) + "_" + DateTime.Now.Hour.ToString("00") + DateTime.Now.Minute.ToString("00") + DateTime.Now.Minute.ToString("00") + "_" + DateTime.Now.Millisecond.ToString("000") + "_";
            var fileName = dmy + Path.GetFileName(doc.FileName.Replace('#', ' ').Replace('%', ' ').Replace('+', ' '));

            var path = Path.Combine(Server.MapPath(subPath), fileName);
            doc.SaveAs(path);
            string string_path = subPath + fileName;
            return string_path;

        }

        public List<List<object>> ReadDataFromExcel(string excelFileRelativePath, int beginRow = 2, int beginColumn = 1, int worksheetNumber = 1)
        {
            if (!excelFileRelativePath.Contains("~"))
            {
                excelFileRelativePath = "~/" + excelFileRelativePath;
            }
            var existingFile = new FileInfo(Server.MapPath(excelFileRelativePath));
            var excelList = new List<List<object>>();

            using (ExcelPackage package = new ExcelPackage(existingFile))
            {
                ExcelWorksheet worksheet = package.Workbook.Worksheets[worksheetNumber];
                var allRow = worksheet.Dimension.End.Row;
                var allColumn = 8;// worksheet.Dimension.End.Column;
                var format = string.Empty;
                double tryDouble;
                DateTime tryDateTime;

                for (var row = beginRow; row <= allRow; row++)
                {
                    // Check Empty Row
                    var isEmptyRow = true;
                    for (int column = beginColumn; column <= allColumn; column++)
                    {
                        if (worksheet.Cells[row, column] != null && worksheet.Cells[row, column].Value != null && worksheet.Cells[row, column].Text.Trim() != string.Empty)
                        {
                            isEmptyRow = false;
                        }
                    }

                    if (!isEmptyRow)
                    {
                        var rowList = new List<object>();
                        for (int column = beginColumn; column <= allColumn; column++)
                        {
                            format = worksheet.Cells[row, column].Style.Numberformat.Format;
                            if (format == "General" || format == "@")
                            {
                                if (format == "General" && worksheet.Cells[row, column].Value != null && worksheet.Cells[row, column].Value.ToString().Trim() == "0")
                                {
                                    rowList.Add(worksheet.Cells[row, column].Value);
                                }
                                else
                                {
                                    rowList.Add(worksheet.Cells[row, column].Text.Trim());
                                }
                            }
                            else if (format.Contains("d") && format.Contains("m") && format.Contains("y") && worksheet.Cells[row, column].Value != null)
                            {
                                if (double.TryParse(worksheet.Cells[row, column].Value.ToString(), out tryDouble))
                                {
                                    rowList.Add(DateTime.FromOADate(tryDouble));
                                }
                                else if (DateTime.TryParse(worksheet.Cells[row, column].Value.ToString(), out tryDateTime))
                                {
                                    rowList.Add(tryDateTime);
                                }
                                else
                                {
                                    rowList.Add(worksheet.Cells[row, column].Value.ToString());
                                }
                            }
                            else if (format.Contains("#") || format.Contains(".") || format.Contains("0"))
                            {
                                rowList.Add(worksheet.Cells[row, column].Value);
                            }
                            else
                            {
                                rowList.Add(worksheet.Cells[row, column].Text);
                            }
                        }
                        excelList.Add(rowList);
                    }
                }
            }

            return excelList;
        }

        public int CheckDuplicateOrderRoute(string orderDate)
        {
            string query = "select * from sampleorder where orderdate='" + orderDate + "'";
            var getData = mysqlCon.GetDataTable(query, "sampleorder");
            return getData.Rows.Count;
        }

        public void DeleteDuplicateOrderRoute(string orderDate)
        {
            string query = "delete from sampleorder where orderdate='" + orderDate + "'";
            mysqlCon.Execute(query);
        }

        public void ModifyExcel<T>(string folder, string fileName, Dictionary<string, object> fixedCellValue = null, List<T> query = null, List<T> query2 = null, List<string> header = null, List<int> summaryColumn = null, int rowBeginHead = 1, int worksheetNumber = 1)
        {
            using (ExcelPackage pkg = new ExcelPackage())
            {
                var fullFilename = fileName.Contains(".xlsx") ? fileName : fileName + ".xlsx";
                var oldFilePath = Server.MapPath((folder.Contains("~/") ? folder : "~/" + folder) + "/" + fullFilename);
                using (FileStream stream = new FileStream(oldFilePath, FileMode.Open))
                {
                    for (int j = 1; j <= worksheetNumber; j++)
                    {
                        pkg.Load(stream);
                        ExcelWorksheet ws = pkg.Workbook.Worksheets[j];

                        //set Fixed Cell value
                        if (fixedCellValue != null && fixedCellValue.Count > 0)
                        {
                            foreach (var item in fixedCellValue)
                            {
                                ws.Cells[item.Key].Value = item.Value;
                            }
                        }

                        if (query != null && query.Count > 0)
                        {
                            //get our column headings
                            var colHeadCount = 0;
                            if (header != null && header.Count > 0)
                            {
                                for (int i = 0; i < header.Count; i++)
                                {
                                    ws.Cells[rowBeginHead, i + 1].Value = header[i];
                                    colHeadCount++;
                                }

                                //Format the header
                                using (ExcelRange rng = ws.Cells[rowBeginHead, 1, rowBeginHead, colHeadCount])
                                {
                                    rng.Style.Font.Bold = true;
                                    rng.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                    rng.Style.Border.Bottom.Style = rng.Style.Border.Left.Style = rng.Style.Border.Right.Style = rng.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    rng.Style.Fill.PatternType = ExcelFillStyle.Solid;                      //Set Pattern for the background to Solid
                                    rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(79, 129, 189));  //Set color to dark blue
                                    rng.Style.Font.Color.SetColor(Color.White);
                                }
                            }

                            //populate our Data from List
                            var rowBeginList = rowBeginHead + 1;
                            var range = j == 1 ? ws.Cells["A" + rowBeginList].LoadFromCollection(query) : ws.Cells["A" + rowBeginList].LoadFromCollection(query2);
                            range.Style.Border.Bottom.Style = range.Style.Border.Left.Style = range.Style.Border.Right.Style = range.Style.Border.Top.Style = ExcelBorderStyle.Thin;

                            //summary Field
                            if (summaryColumn != null && summaryColumn.Count > 0)
                            {
                                var summaryRow = query.Count + rowBeginList;
                                foreach (var item in summaryColumn)
                                {
                                    ws.Cells[summaryRow, item].Formula = "Sum(" + ws.Cells[rowBeginList, item].Address + ":" + ws.Cells[summaryRow - 1, item].Address + ")";
                                    ws.Cells[summaryRow, item].Style.Font.Bold = true;
                                }
                            }
                        }
                    }

                    //Write it back to the client
                    Random rand = new Random();
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", "attachment;  filename=" + DateTime.Now.ToString("yyyyMMdd") + "_" + rand.Next(1, 999) + "_" + fullFilename);
                    Response.BinaryWrite(pkg.GetAsByteArray());
                    Response.End();

                }
            }
        }

    }
}