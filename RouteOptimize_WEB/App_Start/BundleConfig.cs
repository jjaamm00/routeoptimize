﻿using System.Web;
using System.Web.Optimization;

namespace RouteOptimize_WEB
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/jtable/jquery.dataTables.min.js",
                      "~/Scripts/jtable/dataTables.bootstrap4.min.js",
                      "~/Scripts/jtable/dataTables.fixedHeader.min.js",
                      "~/Scripts/noty/packaged/jquery.noty.packaged.js",
                      "~/Scripts/bootstrap-datepicker/js/bootstrap-datepicker.min.js",
                      "~/Scripts/bootstrap-datepicker/locales/bootstrap-datepicker.en-GB.min.js",
                      "~/Scripts/moment.min.js",
                      "~/Scripts/moment-with-locales.js"
                      ));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/jtable/fixedHeader.bootstrap4.min.css",
                      "~/Content/jtable/dataTables.bootstrap4.min.css",
                      "~/Scripts/bootstrap-datepicker/css/bootstrap-datepicker.min.css"
                      ));
        }
    }
}
