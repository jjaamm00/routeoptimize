﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RouteOptimize_WEB.Models
{
    public enum ReturnType
    {
        DataSet,
        DataTable,
        Int
    }
}