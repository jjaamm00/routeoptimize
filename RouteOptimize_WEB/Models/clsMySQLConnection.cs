﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;

namespace RouteOptimize_WEB.Models
{

    class clsMySQLConnection
    {
        string strCon = ConfigurationManager.ConnectionStrings["DISPOPRD"].ConnectionString;
        //string strCon = "";

        public DataTable GetDataTable(string strSQL, string TableName = "Table", string strConString = "")
        {
            try
            {
                if (strConString == "") strConString = strCon;
                DataTable dt;
                using (MySqlConnection connection = new MySqlConnection(strConString))
                {
                    connection.Open();
                    using (var da = new MySqlDataAdapter(strSQL, connection))
                    {
                        dt = new DataTable(TableName);
                        da.Fill(dt);
                    }
                }
                return dt;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public int Execute(string strSQL, string strConString = "")
        {
            int intEffectRow;
            try
            {
                if (strConString == "") strConString = strCon;
                using (MySqlConnection connection = new MySqlConnection(strConString))
                {
                    connection.Open();
                    using (MySqlCommand cmd = new MySqlCommand(strSQL, connection))
                    {
                        intEffectRow = cmd.ExecuteNonQuery();
                    }
                }

                return intEffectRow;
            }
            catch (Exception)
            {
                return -1;
            }
        }

        public string ExecuteScalar(string strSQL, string strConString = "")
        {
            try
            {
                if (strConString == "") strConString = strCon;
                using (MySqlConnection connection = new MySqlConnection(strConString))
                {
                    connection.Open();
                    using (MySqlCommand cmd = new MySqlCommand(strSQL, connection))
                    {
                        cmd.ExecuteScalar();
                    }
                }

                return "";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string SaveDataTable(string strSQL, DataTable dtName, string strConString = "")
        {
            try
            {
                if (strConString == "") strConString = strCon;
                using (MySqlConnection connection = new MySqlConnection(strConString))
                {
                    connection.Open();
                    using (var da = new MySqlDataAdapter(strSQL, connection))
                    {
                        MySqlCommandBuilder db = new MySqlCommandBuilder(da);
                        da.Update(dtName);
                        dtName.AcceptChanges();
                    }
                }

                return "";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public DateTime GetDate(string strConString = "")
        {
            try
            {
                DataTable dt = GetDataTable("Select date(Now()) as CurrentDate;", "Date", strConString);
                if (dt.Rows.Count > 0)
                {
                    DataRow dr = dt.Rows[0];
                    return (DateTime)(dr["CurrentDate"]);
                }
                else
                {
                    return DateTime.Parse("01/01/1900");
                }
            }
            catch (Exception)
            {
                return DateTime.Parse("01/01/1900");
            }
        }

        public DateTime GetFullDate(string strConString = "")
        {
            try
            {
                DataTable dt = GetDataTable("Select Now() as CurrentDate;", "Date", strConString);
                if (dt.Rows.Count > 0)
                {
                    DataRow dr = dt.Rows[0];
                    return (DateTime)(dr["CurrentDate"]);
                }
                else
                {
                    return DateTime.Parse("01/01/1900 00:00:00");
                }
            }
            catch (Exception)
            {
                return DateTime.Parse("01/01/1900 00:00:00");
            }
        }

    }
}